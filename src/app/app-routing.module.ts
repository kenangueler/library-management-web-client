import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {HomePageComponent} from "./pages/home-page/home-page.component";
import {PageNotFoundComponent} from "./pages/page-not-found/page-not-found.component";
import {BooksPageComponent} from "./pages/books-page/books-page.component";
import {BorrowingsPageComponent} from "./pages/borrowings-page/borrowings-page.component";
import {AccountPageComponent} from "./pages/account-page/account-page.component";

const routes: Routes = [
  {path: '', redirectTo: '/home', pathMatch: 'full'},
  {path: 'home', component: HomePageComponent},
  {path: 'books', component: BooksPageComponent},
  {path: 'borrowings', component: BorrowingsPageComponent},
  {path: 'account', component: AccountPageComponent},
  {path: '**', component: PageNotFoundComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes, {useHash: false})],
  exports: [RouterModule]
})
export class AppRoutingModule {
}
