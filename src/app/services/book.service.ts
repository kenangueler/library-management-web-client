import { Injectable } from '@angular/core';
import {Book} from "../data/book";
import {HttpClient} from "@angular/common/http";
import {environment} from "../../environments/environment";
import {Observable} from "rxjs";

@Injectable({
  providedIn: 'root'
})
export class BookService {
  constructor(private $http: HttpClient) { }

  getBooks(): Observable<Book[]> {
    return this.$http.get<Book[]>(`${environment.apiBaseUrl}/books`);
  }
}
