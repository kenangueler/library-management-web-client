import {Author} from "./author";
import {Publisher} from "./publisher";

export class Book {
  public id!: number;
  public isbn10!: string;
  public isbn13!: string;
  public title!: string;
  public subtitle!: string;
  public language!: string;
  public pageCount!: number;
  public publishedYear!: number;
  public authors!: Author[];
  public genres!: string[];
  public publisher!: string;
  public totalAmount!: number;
  public totalAvailable!: number;
  public description!: string;
  public averageRating!: number;
  public ratingsCount!: number;
  public thumbnail!: string;
  public printType!: string;
}
